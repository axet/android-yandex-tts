/*
 * Copyright (C) 2011 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.axet.yandextts.app;

import android.annotation.TargetApi;
import android.content.Context;
import android.media.AudioFormat;
import android.media.MediaCodec;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.net.Uri;
import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.util.Log;

import com.github.axet.yandextts.activities.CheckVoiceData;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

public class YandexTTS {
    private static final String TAG = YandexTTS.class.getSimpleName();

    // https://cloud.yandex.ru/ru/docs/speechkit/formats
    public static String FORMAT_MP3 = "mp3";
    public static String FORMAT_OGG = "oggopus";
    public static String FORMAT_LPCM = "lpcm"; // 16bit, signed integer, little-endian

    public static int TEXT_MAX = 5000;

    private final Context mContext;
    private final SynthReadyCallback mCallback;
    String name;

    private boolean mInitialized = false;

    public YandexTTS(Context context, SynthReadyCallback callback) {
        mContext = context;
        mCallback = callback;

        attemptInit();
    }

    @Override
    protected void finalize() {
    }

    public int getSampleRate() {
        return 48000;
    }

    public int getChannelCount() {
        return 1;
    }

    public int getAudioFormat() {
        return AudioFormat.ENCODING_PCM_16BIT;
    }

    public List<Voice> getAvailableVoices() {
        final List<Voice> voices = new LinkedList<Voice>();

        voices.add(new Voice("en_US", "yandex", 0, 20));
        voices.add(new Voice("ru_RU", "yandex", 0, 20));

        return voices;
    }

    public void setVoiceByProperties(String name, String languages, int gender, int age, int variant) {
        this.name = languages;
    }

    public void setLanguage(String language, int variant) {
        attemptInit();
    }

    public void setRate(int rate) {
    }

    public void setPitch(int pitch) {
    }

    @TargetApi(16)
    public void decode(Uri uri, ByteArrayOutputStream bos) throws IOException {
        Log.d("TTS", "Decoding " + uri);
        MediaExtractor extractor = new MediaExtractor();
        extractor.setDataSource(mContext, uri, null);
        MediaCodec decoder = null;

        for (int i = 0; i < extractor.getTrackCount(); i++) {
            MediaFormat format = extractor.getTrackFormat(i);
            String mime = format.getString(MediaFormat.KEY_MIME);
            if (mime.startsWith("audio/")) {
                extractor.selectTrack(i);
                try {
                    decoder = MediaCodec.createDecoderByType(mime);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
                decoder.configure(format, null, null, 0);
                break;
            }
        }

        if (decoder == null)
            throw new RuntimeException("Can't find video info!");

        decoder.start();

        ByteBuffer[] inputBuffers = decoder.getInputBuffers();
        ByteBuffer[] outputBuffers = decoder.getOutputBuffers();
        MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();
        boolean isEOS = false;

        while (!Thread.interrupted()) {
            if (!isEOS) {
                int inIndex = decoder.dequeueInputBuffer(10000);
                if (inIndex >= 0) {
                    ByteBuffer buffer = inputBuffers[inIndex];
                    int sampleSize = extractor.readSampleData(buffer, 0);
                    if (sampleSize < 0) {
                        Log.d("DecodeActivity", "InputBuffer BUFFER_FLAG_END_OF_STREAM");
                        decoder.queueInputBuffer(inIndex, 0, 0, 0, MediaCodec.BUFFER_FLAG_END_OF_STREAM);
                        isEOS = true;
                    } else {
                        decoder.queueInputBuffer(inIndex, 0, sampleSize, extractor.getSampleTime(), 0);
                        extractor.advance();
                    }
                }
            }

            int outIndex = decoder.dequeueOutputBuffer(info, 10000);
            switch (outIndex) {
                case MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED:
                    outputBuffers = decoder.getOutputBuffers();
                    break;
                case MediaCodec.INFO_TRY_AGAIN_LATER:
                    break;
                case MediaCodec.INFO_OUTPUT_FORMAT_CHANGED:
                    MediaFormat f = decoder.getOutputFormat();
                    if (f.getInteger(MediaFormat.KEY_SAMPLE_RATE) != getSampleRate() ||
                            f.getInteger(MediaFormat.KEY_CHANNEL_COUNT) != getChannelCount() ||
                            f.getInteger(MediaFormat.KEY_PCM_ENCODING) != getAudioFormat())
                        throw new RuntimeException("wrong hardcoded values");
                    break;
                default:
                    ByteBuffer buffer = outputBuffers[outIndex];

                    byte[] b = new byte[info.size - info.offset];
                    buffer.get(b);
                    bos.write(b);

                    decoder.releaseOutputBuffer(outIndex, true);
                    break;
            }

            if ((info.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0)
                break;
        }

        decoder.stop();
        decoder.release();
        extractor.release();
    }

    public boolean decode(Uri uri, int m) {
        for (int i = 0; i < m; i++) {
            try {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                decode(uri, bos);
                if (bos.size() == 0)
                    throw new RuntimeException("empty output");
                nativeSynthCallback(bos.toByteArray());
                nativeSynthCallback(null);
                return true;
            } catch (Exception e) {
                Log.e(TAG, "decode error #" + i, e);
            }
        }
        return false;
    }

    boolean stream(Uri uri, int m) {
        for (int i = 0; i < m; i++) {
            try {
                InputStream is = mContext.getContentResolver().openInputStream(uri);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                byte[] buf = new byte[1024];
                int len;
                while ((len = is.read(buf)) > 0)
                    bos.write(buf, 0, len);
                nativeSynthCallback(bos.toByteArray());
                nativeSynthCallback(null);
                return true;
            } catch (Exception e) {
                Log.e(TAG, "download error", e);
            }
        }
        return false;
    }

    public void synthesize(String text) {
        text = text.substring(0, Math.min(text.length(), TEXT_MAX));
        if (Build.VERSION.SDK_INT >= 16) {
            Uri uri = Uri.parse("http://tts.voicetech.yandex.net/tts")
                    .buildUpon()
                    .appendQueryParameter("quality", "hi")
                    .appendQueryParameter("format", FORMAT_MP3)
                    .appendQueryParameter("sampleRateHertz", String.valueOf(getSampleRate()))
                    .appendQueryParameter("text", text)
                    .appendQueryParameter("lang", "ru_RU")
                    .build();
            if (decode(uri, 2))
                return;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                return;
            }
            if (decode(uri, 2))
                return;
            mCallback.onSynthError(TextToSpeech.ERROR_NETWORK);
        } else {
            Uri uri = Uri.parse("http://tts.voicetech.yandex.net/tts")
                    .buildUpon()
                    .appendQueryParameter("quality", "hi")
                    .appendQueryParameter("format", FORMAT_LPCM)
                    .appendQueryParameter("sampleRateHertz", String.valueOf(getSampleRate()))
                    .appendQueryParameter("text", text)
                    .appendQueryParameter("lang", "ru_RU")
                    .build();
            if (stream(uri, 2))
                return;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                return;
            }
            if (stream(uri, 2))
                return;
            mCallback.onSynthError(TextToSpeech.ERROR_NETWORK);
        }
    }

    public void stop() {
    }

    @SuppressWarnings("unused")
    private void nativeSynthCallback(byte[] audioData) {
        if (mCallback == null)
            return;

        if (audioData == null) {
            mCallback.onSynthDataComplete();
        } else {
            mCallback.onSynthDataReady(audioData);
        }
    }

    private void attemptInit() {
        if (mInitialized) {
            return;
        }

        if (!CheckVoiceData.hasBaseResources(mContext)) {
            Log.e(TAG, "Missing base resources");
            return;
        }

        Log.i(TAG, "Initialized synthesis library with sample rate = " + getSampleRate());

        mInitialized = true;
    }

    public interface SynthReadyCallback {
        void onSynthDataReady(byte[] audioData);

        void onSynthDataComplete();

        void onSynthError(int errorCode); // @TextToSpeech.Error
    }

    public class Voice {
        public final String name;
        public final String identifier;
        public final int gender;
        public final int age;
        public final Locale locale;

        public Voice(String name, String identifier, int gender, int age) {
            this.name = name;
            this.identifier = identifier;
            this.gender = gender;
            this.age = age;

            locale = new Locale(name);
        }

        /**
         * Attempts a partial match against a query locale.
         *
         * @param query The locale to match.
         * @return A text-to-speech availability code. One of:
         * <ul>
         * <li>{@link TextToSpeech#LANG_NOT_SUPPORTED}
         * <li>{@link TextToSpeech#LANG_AVAILABLE}
         * <li>{@link TextToSpeech#LANG_COUNTRY_AVAILABLE}
         * <li>{@link TextToSpeech#LANG_COUNTRY_VAR_AVAILABLE}
         * </ul>
         */
        public int match(Locale query) {
            if (!locale.getISO3Language().equals(query.getISO3Language())) {
                return TextToSpeech.LANG_NOT_SUPPORTED;
            } else if (!locale.getISO3Country().equals(query.getISO3Country())) {
                return TextToSpeech.LANG_AVAILABLE;
            } else if (!locale.getVariant().equals(query.getVariant())) {
                return TextToSpeech.LANG_COUNTRY_AVAILABLE;
            } else {
                return TextToSpeech.LANG_COUNTRY_VAR_AVAILABLE;
            }
        }

        @Override
        public String toString() {
            return name;
        }
    }
}
