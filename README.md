# Yandex TTS

Simple REST yandex TTS engine for android.

This service can be installed as system Android TTS engine.

Based on ESpeak sources: https://code.google.com/archive/p/eyes-free/source

# Supported Languages

Currently only supports "ru_RU", but will also pronounce "en_US" words using Russian pronounciation.

# Limitations

* Requires constant internet connection.
* Maybe discontinued any moment by Yandex.

# Links

* https://cloud.yandex.ru/ru/docs/speechkit/tts/
